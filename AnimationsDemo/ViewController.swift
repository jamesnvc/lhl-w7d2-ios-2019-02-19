//
//  ViewController.swift
//  AnimationsDemo
//
//  Created by James Cash on 19-02-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var purpleView: UIView!

    var widthConstraint: NSLayoutConstraint!
    var yConstraint: NSLayoutConstraint!
    var centerXConstraint: NSLayoutConstraint!
    var leftXConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        let redView = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        redView.backgroundColor = UIColor.red

        let label = UILabel(frame: CGRect(x: 0, y: 50, width: 150, height: 44))
        label.text = "Hello"
        redView.addSubview(label)

        let button = UIButton(type: .system)
        button.frame = label.frame
        button.setTitle("Button", for: .normal)
        button.addTarget(self, action: #selector(movePurple(_:)), for: .touchUpInside)

        view.addSubview(redView)

        //swap(old: label, new: button)

        UIView.animateKeyframes(
            withDuration: 3.0, delay: 0, options: [],
            animations: {
                UIView.addKeyframe(
                    withRelativeStartTime: 0,
                relativeDuration: 0.25,
                animations: {
                    redView.transform = CGAffineTransform(rotationAngle: 0.5*CGFloat.pi)
                    redView.center = CGPoint(x: 200, y: 200)
                    redView.backgroundColor = UIColor.purple
                })
                UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.5, animations: {
                    redView.transform = CGAffineTransform(rotationAngle: 1.5*CGFloat.pi)
                    redView.center = CGPoint(x: 300, y: 400)
                    redView.backgroundColor = UIColor.green
                })
                UIView.addKeyframe(withRelativeStartTime: 0.75,
                                   relativeDuration: 0.25,
                                   animations: {
                                    redView.transform = CGAffineTransform(rotationAngle: 2*CGFloat.pi)
                                    redView.center = CGPoint(x: 100, y: 250)
                                    redView.backgroundColor = UIColor.red
                })
            },
            completion: { (isDone: Bool) in
                print("Finished \(isDone)")
                self.swap(old: label, new: button)
            }
        )

        let view2 = UIView(frame: CGRect.zero)
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.backgroundColor = UIColor.purple
        view.addSubview(view2)
        widthConstraint =             view2.widthAnchor.constraint(equalToConstant: 200)
        yConstraint =            view2.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        centerXConstraint =             view2.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        leftXConstraint = view.leftAnchor.constraint(equalTo: view.leftAnchor)
        NSLayoutConstraint.activate([
            widthConstraint,
            view2.heightAnchor.constraint(equalToConstant: 200),
            centerXConstraint,
            yConstraint
        ])
        purpleView = view2

    }

    @IBAction func movePurple(_ sender: UIView) {
        UIView.animate(withDuration: 5) {
//            self.purpleView.frame = CGRect(x: 100, y: 100, width: 500, height: 100)
            self.widthConstraint.constant = 300
            self.yConstraint.constant = 300
            self.centerXConstraint.isActive = false
            self.leftXConstraint.isActive = true
            self.view.layoutSubviews()
        }
    }

    func swap(old: UIView, new: UIView) {
        if UIAccessibility.isReduceMotionEnabled {
            let superView = old.superview!
            old.removeFromSuperview()
            superView.addSubview(new)
            superView.setNeedsDisplay()
        } else {
            UIView.transition(from: old, to: new, duration: 3,
                              options: [.transitionFlipFromTop]) { (done) in
                                print("Finished adding new view")
            }
        }
    }

}

